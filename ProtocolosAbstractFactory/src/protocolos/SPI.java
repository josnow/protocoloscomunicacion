package protocolos;

public class SPI extends Protocolo {

	private String direccion;
	private int velocidad =20;
	private int esclavos = 20;
	
	@Override
	public String getPropiedades() {
		return "SPI \nVelocidad: "+velocidad;
	}

	@Override
	public void setDireccion(String dir) {
		
		this.direccion = dir;
	}

}
