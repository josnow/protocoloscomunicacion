package graficos;

import java.util.Scanner;
import protocolos.*;

public class User {

	public static void main(String[] args) {
		
		Protocolo protocolo = null;
		Modulo modulo = null;
		Scanner lector = new Scanner(System.in);
		
		System.out.println("Seleccione el protocolo a usar: ");
		System.out.println("1. SPI.");
		System.out.println("2. I2C.");
		System.out.println("3. RS485");
		
		int pro = lector.nextInt();
		
		switch (pro) {
		
		case 1:
			protocolo = new SPI();
		break;
		case 2:
			protocolo = new I2C();
		break;
		case 3:
			protocolo = new RS485();
		break;
		}
		
		System.out.println("Digite la direccion del modulo: ");
		
		String dir = lector.next();
		protocolo.setDireccion(dir);
		
		System.out.println("Seleccione el modulo a conectar: ");
		System.out.println("1. Wifi.");
		System.out.println("2. LCD.");	
		
		pro = lector.nextInt();
		
		switch (pro) {
		
		case 1:
			modulo = new Wifi();
		break;
		case 2:
			modulo = new LCD();
		break;
		default:
			System.out.println("Modulo no disponible");
		break;
		}
		
		modulo.setProtocolo(protocolo);
	}

}
